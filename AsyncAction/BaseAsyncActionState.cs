﻿using UnityEngine;
using UnityEditor;


namespace xres
{

    public static class BaseAsyncActionState
    {

        public const string Init = "Init";
        
        public const string Finish = "Finish";

    } //end of class

}
