# xRes

#### 介绍
Unity下的一个资源加载框架，整体思路参考自xRes和KSFramework

#### 已实现功能

1. 首次启动解压资源
2. 资源热更
3. 资源加载(同步, 异步)
4. 资源内存管理

#### 待实现功能

1. 资源分包
2. 资源加密
3. 与FairyGUI集成
4. 增加开发模式, 修改完直接运行看效果, 减少AssetBundle构建次数


#### 使用说明

1. 初始化

```
xRes.Initialize();
//如果是开发模式, 可以把ResPath.Editor_Hotfix_Enable这个常量设为false, 这样不需要走首次启动解压资源的流程。
//Manifest文件加载完毕后就可以使用xRes进行资源的加载了
xRes.InitManifest().onComplete += OnComplete_Manifest;
```

2. 加载AssetBundle
异步

```
var loader = xRes.LoadBundle("login");
//支持Unity协程和回调的方式监听加载完毕
loader.onComplete.AddListenerWithData(OnComplete, data);
```

同步

```
var bundle = xRes.GetBundle("login");
```


3. 加载资源
异步

```
var loader = xRes.LoadAsset("login", "background", typeof(UnityEngine.Sprite));
//支持Unity协程和回调的方式监听加载完毕
loader.onComplete.AddListenerWithData(OnComplete, data);
```

同步

```
var asset = xRes.GetAsset<UnityEngine.Sprite>("login", "background");
```

4.资源管理
```
//使用引用计数的方式来管理资源是否可被释放
var loader = xRes.LoadBundle("login");
loader.Retain(uiTag);
//...
loader.Release(uiTag);

xRes.UnloadUnused();
xRes.Unload("login");
```



