﻿using UnityEngine;
using UnityEditor;


namespace xres
{


    public class FileVersion
    {
        public static FileVersion Parse(string s)
        {
            var fv = new FileVersion();
            fv.ParseFrom(s);
            return fv;
        }


        /// <summary>
        /// 相对路径
        /// </summary>
        public string path = "";

        public long len;

        public uint crc;

        public long offset;

        public FileVersion()
        {
        }

        public FileVersion(string path, long len)
        {
            this.path = path;
            this.len = len;
        }

        public FileVersion ParseFrom(string s)
        {
            var arr = s.Split('|');
            
            path = arr[0];
            len = long.Parse(arr[1]);
            crc = uint.Parse(arr[2]);
            if (arr.Length > 3)
            {
                //var offset = arr[3];
            }
            else
            {
                offset = 0;
            }

            return this;
        }

    }


}
