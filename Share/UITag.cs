﻿

namespace xuni
{


    public sealed class UITag
    {

        static Pool<UITag> s_pool;

        public static void Recycle(UITag tag)
        {
            if (null == s_pool || null == tag)
                return;

            s_pool.Recycle(tag);
        }

        public static UITag Obtain()
        {
            if (null == s_pool)
                s_pool = new Pool<UITag>((sender) => new UITag(), (sender, tag) => { tag.name = ""; });

            return s_pool.Obtain();
        }

        public static UITag Obtain(string name)
        {
            if (null == s_pool)
                s_pool = new Pool<UITag>((sender) => new UITag(), (sender, tag) => { tag.name = ""; });

            var ret = s_pool.Obtain();
            ret.name = name;
            return ret;
        }

        public string name = "";


    } //end of class


}

