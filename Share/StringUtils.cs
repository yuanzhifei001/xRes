﻿using System.Text;

namespace xuni
{


    public static class StringUtils
    {

        public readonly static StringBuilder Builder = new StringBuilder();

        public static string BuildString(this StringBuilder sb)
        {
            var str = sb.ToString();
            sb.Clear();
            return str;
        }


    } //end of class

}
