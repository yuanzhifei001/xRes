﻿

namespace xuni
{
    

    /// <summary>
    /// 用于存放简单的上下文自定义数据
    /// </summary>
    public class UserData
    {

        public int i;
        public string s = "";
        public object obj;

    } //end of class


}