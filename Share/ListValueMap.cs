﻿using System.Collections.Generic;

namespace xuni
{


    /// <summary>
    /// 键值是List的Dictionary
    /// </summary>
    public class ListValueMap<T_Key, T_Value>
    {
        public readonly Dictionary<T_Key, List<T_Value>> Map = new Dictionary<T_Key, List<T_Value>>();

        public List<T_Value> this[T_Key key]
        {
            get
            {
                return Map[key];
            }
        }

        public void AddOne(T_Key key, T_Value value)
        {
            if (!Map.TryGetValue(key, out var list))
            {
                list = new List<T_Value>();
                Map.Add(key, list);
            }

            list.Add(value);
        }

        public bool TryGetList(T_Key key, out List<T_Value> value)
        {
            if (Map.TryGetValue(key, out value))
                return true;

            return false;
        }

        public bool RemoveOne(T_Key key, T_Value value)
        {
            if (Map.TryGetValue(key, out var list))
            {
                return list.Remove(value);
            }

            return false;
        }

        public bool RemoveKey(T_Key key)
        {
            return Map.Remove(key);
        }

        public bool ContainsKey(T_Key key)
        {
            return Map.ContainsKey(key);
        }

        public void Clear()
        {
            Map.Clear();
        }

        public Dictionary<T_Key, List<T_Value>>.Enumerator GetEnumerator()
        {
            return Map.GetEnumerator();
        }

    } //end of class

}
