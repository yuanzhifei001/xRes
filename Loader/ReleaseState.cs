﻿
namespace xres
{


    public static class ReleaseState
    {

        public const string Init = "Init";

        /// <summary>
        /// 等待释放
        /// </summary>
        public const string Wait = "Wait";

        //public const string Released = "Released";

    } //end of class


}
