﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace xres
{


    public static class LoaderState
    {

        public const string Init = BaseAsyncActionState.Init;

        public const string LoadAssetBundle = "LoadAssetBundle";

        public const string LoadAsset = "LoadAsset";

        public const string Finish = BaseAsyncActionState.Finish;

        //public const string WaitUnload = "WaitUnload";

        //PendingUnload,
        //Unload,

    } //end of class


}