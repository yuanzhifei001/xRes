﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using xuni;

namespace xres
{


    public abstract class BaseLoader : BaseAsyncAction
    {

        /// <summary>
        /// 引用计数, 使用页面或UI标识的方式引用计数更直观点, 可以避免多减或多加计数值.
        /// </summary>
        List<UITag> _refList = new List<UITag>();

        float _recentRetainTime;

        int _recentRetainCt;
        float _recentRetainCtResetTime;

        /// <summary>
        /// 内存不足时释放优先级, 值大的优先释放
        /// </summary>
        public int unloadPriority;

        public void Retain(UITag tag)
        {
            if (!_refList.Contains(tag))
            {
                _recentRetainTime = Time.realtimeSinceStartup;

                if (Time.realtimeSinceStartup - _recentRetainCtResetTime > 10 * 60)
                {
                    _recentRetainCtResetTime = Time.realtimeSinceStartup;
                    _recentRetainCt = 0;
                }
                _recentRetainCt++;

                _refList.Add(tag);
            }
        }

        public void Release(UITag tag)
        {
            _refList.Remove(tag);
        }

        public int GetRefCount()
        {
            return _refList.Count;
        }

    } //end of class


}
